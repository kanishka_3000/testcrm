/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef BASETESTSUMMERIZER_H
#define BASETESTSUMMERIZER_H

#include <QObject>
#include <BaseTest.h>
#include <QList>
namespace testcore{
class BaseTestSummerizer : public QObject
{
    Q_OBJECT
public:
    explicit BaseTestSummerizer(QObject *parent = 0);
    void Register(testcore::BaseTest* pTesCase);

signals:

public slots:
    void OnTestComplete(QString sTestName, int iStatus);

private:
    void OnSummeryPrint();
    int i_TotalTestCount;
    int i_CurrentTestNumber;
    QList<QString> lst_FailedTests;
    QList<QString> lst_PassedTests;
};
}
#endif // BASETESTSUMMERIZER_H
