/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "BaseTest.h"
namespace  testcore {


BaseTest::BaseTest(CRMApplication *pApp, QObject *parent) : QObject(parent)
{
    p_App = pApp;
    connect( p_App, SIGNAL(NotifyApplicationReady()),this, SLOT(StartTests()));
}

void BaseTest::StartTests()
{
    int iStatus=  QTest::qExec(this,0, nullptr);
    NotifyTestStatus(metaObject()->className(), iStatus);
}

}
