/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef BASETEST_H
#define BASETEST_H

#include <QObject>
#include <QtTest/QtTest>
#include <QTest>
#include <CRMApplication.h>
namespace testcore {
class BaseTest : public QObject
{
    Q_OBJECT
public:
    explicit BaseTest(CRMApplication* pApp, QObject *parent = 0);

    CRMApplication* p_App;
signals:
    void NotifyTestStatus(QString sTestName, int iStatus);
public slots:
     void StartTests();
};
}
#endif // BASETEST_H
