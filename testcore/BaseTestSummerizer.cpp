/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "BaseTestSummerizer.h"
#include <iostream>
using namespace std;
testcore::BaseTestSummerizer::BaseTestSummerizer(QObject *parent) :
    QObject(parent), i_TotalTestCount(0)
{

}

void testcore::BaseTestSummerizer::Register(testcore::BaseTest *pTesCase)
{
    connect(pTesCase, SIGNAL(NotifyTestStatus(QString,int)), this, SLOT(OnTestComplete(QString,int)));
    i_TotalTestCount++;
}

void testcore::BaseTestSummerizer::OnTestComplete(QString sTestName, int iStatus)
{
    i_CurrentTestNumber++;
    cout << i_CurrentTestNumber << " out of " << i_TotalTestCount << " are complate "<< endl;
    if(iStatus == 0)
    {
        lst_PassedTests.push_back(sTestName);
    }
    else
    {
        lst_FailedTests.push_back(sTestName);
    }
    if(i_CurrentTestNumber == i_TotalTestCount)
    {
        //All tests are complete
        OnSummeryPrint();
    }
}

void testcore::BaseTestSummerizer::OnSummeryPrint()
{
    cout << "****************TEST SUMMERRY****************" << endl;
    cout << lst_PassedTests.count() << " out of " << i_TotalTestCount << "Passed" << endl;
    cout << "*********************************************" << endl;
    if(lst_PassedTests.size() > 0)
    {
        cout << "*************Passed Test Cases***************" << endl;
        for(auto itr = lst_PassedTests.begin(); itr != lst_PassedTests.end(); itr++)
        {
            cout << qPrintable(*itr) << endl;
        }
    }
    cout << "*********************************************" << endl;
    if(lst_FailedTests.size() > 0)
    {
        cout << "*************Failed Test Cases***************" << endl;
        for(auto itr = lst_FailedTests.begin(); itr != lst_FailedTests.end(); itr++)
        {
            cout << qPrintable(*itr) << endl;
        }
    }
    cout << "****************TEST SUMMERRY****************" << endl;

}

