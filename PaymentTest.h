/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef PAYMENTTEST_H
#define PAYMENTTEST_H

#include <QObject>
#include <CRMApplication.h>
#include <testcore/BaseTest.h>
#include <Order.h>
class PaymentTest : public testcore::BaseTest
{
    Q_OBJECT
public:
    explicit PaymentTest(CRMApplication* pApp, QObject *parent = 0);

signals:

private slots:
    void initTestCase();
    void init(){}
    void DoPaymentTest();
    void cleanup(){}
    void cleanupTestCase();

private:
    std::shared_ptr<Order> p_Order;
};

#endif // PAYMENTTEST_H
