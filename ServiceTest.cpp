/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ServiceTest.h"

#include <CRMDBFields.h>
#include <crmglobaldefs.h>
#include <AddServicesWnd.h>
#include <Service.h>
ServiceTest::ServiceTest(CRMApplication *pApp, QObject *parent) :
    testcore::BaseTest(pApp, parent)
{
    p_ServiceGroup = nullptr;
}

void ServiceTest::initTestCase()
{
    p_ServiceGroup = std::static_pointer_cast<ServiceGroup>(p_App->GetEntityFactory()->CreateEntity(TBLNM_CRM_SERVICEGROUP));
    p_ServiceGroup->AddData(FDNM_CRM_SERVICEGROUP_NAME, "TESTGR1");
    p_ServiceGroup->AddData(FDNM_CRM_SERVICEGROUP_DESCRIPTION, "TESTGR1DEscr12 - 343");
    p_ServiceGroup->AddData(FDNM_CRM_SERVICEGROUP_STATE, 2);
    p_ServiceGroup->Save();

    AddServicesWnd* pWnd = p_App->CreateWndEx<AddServicesWnd>(WND_ADE_SERVICE);
    pWnd->SetServiceGroup(p_ServiceGroup);

    QTest::keyClicks(pWnd->p_Ctrl->txt_ServiceName , "TESTSERvicenme1");
    QTest::keyClicks(pWnd->p_Ctrl->txt_Description , "TESTSERvicenme1 Description1");
    pWnd->p_Ctrl->spin_Value->setValue(2585959.32);
    QTest::mouseClick(pWnd->p_Ctrl->btn_Save, Qt::MouseButton::LeftButton);

}

void ServiceTest::DoTest()
{
    QList<std::shared_ptr<Entity>> lstService = Entity::FindInstances(FDNM_CRM_SERVICES_SERVICENAME, "TESTSERvicenme1", TBLNM_CRM_SERVICES);
    QVERIFY(lstService.size() == 1);
    std::shared_ptr<Service> pService = std::static_pointer_cast<Service>(lstService.first());
    QCOMPARE(pService->GetServiceName(), QString( "TESTSERvicenme1"));
    QCOMPARE(pService->GetServiceDescription(), QString( "TESTSERvicenme1 Description1"));
    QCOMPARE(pService->GetValue(), 2585959.32);
}

void ServiceTest::cleanupTestCase()
{
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_SERVICEGROUP, FDNM_CRM_SERVICEGROUP_NAME, "TESTGR1");
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_SERVICES, FDNM_CRM_SERVICES_SERVICENAME, "TESTSERvicenme1");
}

