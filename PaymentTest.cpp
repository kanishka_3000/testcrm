/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "PaymentTest.h"
#include <CRMDBFields.h>
#include <PaymentEntryWnd.h>
PaymentTest::PaymentTest(CRMApplication *pApp, QObject *parent) :
    testcore::BaseTest(pApp, parent)
{

}

void PaymentTest::initTestCase()
{
    p_Order = std::static_pointer_cast<Order>(p_App->GetEntityFactory()->CreateEntity(TBLNM_CRM_CLIENTORDER));
    p_Order->AddData(FDNM_CRM_CLIENTORDER_CUSTOMERID, 800800);
    p_Order->AddData(FDNM_CRM_CLIENTORDER_DELIVERYDATE, "2014-3-23");
    p_Order->AddData(FDNM_CRM_CLIENTORDER_ASIGNEE, "Kan");
    p_Order->AddData(FDNM_CRM_CLIENTORDER_TAKENBY, "Kan2");
    p_Order->AddData(FDNM_CRM_CLIENTORDER_COMMENT, "cmet");
    p_Order->AddData(FDNM_CRM_CLIENTORDER_STATE, 1);
    p_Order->Save();

}

void PaymentTest::DoPaymentTest()
{
    GenericWnd* pW = p_App->CreateWnd(WND_PAYMENT_ENTRY);
    PaymentEntryWnd* pPayementEndWnd = static_cast<PaymentEntryWnd*>(pW);
    pPayementEndWnd->InitalizeWithOrder(p_Order);

    //QTest::keyClicks(pPayementEndWnd->p_Ctrl->spin_Amount, "123456.56");
    pPayementEndWnd->p_Ctrl->spin_Amount->setValue(123456.56);
    QTest::keyClicks(pPayementEndWnd->p_Ctrl->txt_PaymentDate , "2015-4-21");
    QTest::keyClicks(pPayementEndWnd->p_Ctrl->txt_Reason, "Init");
    QTest::keyClicks(pPayementEndWnd->p_Ctrl->txt_TakenBy , "Admin");

    QTest::mouseClick(pPayementEndWnd->p_Ctrl->btn_Submit, Qt::MouseButton::LeftButton);

    QList<std::shared_ptr<Entity>> lstPayment = Entity::FindInstances(FDNM_CRM_PAYMENT_ORDERID, p_Order->GetOrderID(), TBLNM_CRM_PAYMENT);

    QVERIFY(lstPayment.size() == 1);

    std::shared_ptr<Payment> pPayment = std::static_pointer_cast<Payment>(lstPayment.first());
    double dVal = pPayment->GetAmount();
    int iVal1 = (dVal * 100);
    int iVal2 = (123456.56 * 100);

    QCOMPARE(iVal1, iVal2);
    //QCOMPARE(pPayment->GetData(FDNM_CRM_PAYMENT_DATE).toString(), QString("2015-4-21"));
    QCOMPARE(pPayment->GetData(FDNM_CRM_PAYMENT_REASON).toString(), QString("Init"));
    QCOMPARE(pPayment->GetData(FDNM_CRM_PAYMENT_TAKENBY).toString(), QString("Admin"));
}

void PaymentTest::cleanupTestCase()
{
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_PAYMENT, FDNM_CRM_PAYMENT_ORDERID, p_Order->GetOrderID());
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_CLIENTORDER, FDNM_CRM_CLIENTORDER_CUSTOMERID, "800800");
}

