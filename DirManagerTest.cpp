/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "DirManagerTest.h"

DirManagerTest::DirManagerTest(CRMApplication *pApp, QObject *parent) :
    testcore::BaseTest(pApp, parent)
{
    // Clean();
}

void DirManagerTest::initTestCase()
{

    s_CurrentPath = QDir::currentPath();
    QStringList lstChildren;
    lstChildren << "SOURCE" << "PROCESSING" << "COMPLTE";
    p_DirManager = new feature::DirectoryManager(s_CurrentPath, "BASEDIR", lstChildren);
}

void DirManagerTest::DoPathGenTest()
{

    QCOMPARE( p_DirManager->s_BaseDir, QString(s_CurrentPath + "/BASEDIR"));
    QCOMPARE( p_DirManager->s_SubDirs.at(0), QString(s_CurrentPath + "/BASEDIR/SOURCE"));
    QCOMPARE( p_DirManager->s_SubDirs.at(1), QString(s_CurrentPath + "/BASEDIR/PROCESSING"));
    QCOMPARE( p_DirManager->s_SubDirs.at(2), QString(s_CurrentPath + "/BASEDIR/COMPLTE"));
}

void DirManagerTest::DoPathCreationTest()
{
    bool bCreate = p_DirManager->Create();
    QVERIFY(bCreate == true);

    QString sBase = QString(s_CurrentPath + "/BASEDIR");
    QDir oDir(sBase);
    QVERIFY(oDir.exists());

    QString s1 = QString(s_CurrentPath + "/BASEDIR/SOURCE");
    QString s2 = QString(s_CurrentPath + "/BASEDIR/PROCESSING");
    QString s3 = QString(s_CurrentPath + "/BASEDIR/COMPLTE");

    QDir oD1(s1);
    QDir oD2(s2);
    QDir oD3(s3);
    QVERIFY(oD1.exists());
    QVERIFY(oD2.exists());
    QVERIFY(oD3.exists());
}

void DirManagerTest::OpneDirTest()
{
    QVERIFY(p_DirManager->Open("SOURCE"));
}

void DirManagerTest::Clean()
{
    QString s1 = QString(s_CurrentPath + "/BASEDIR/SOURCE");
    QString s2 = QString(s_CurrentPath + "/BASEDIR/PROCESSING");
    QString s3 = QString(s_CurrentPath + "/BASEDIR/COMPLTE");

    QDir oD1(s1);
    QDir oD2(s2);
    QDir oD3(s3);

    oD1.rmpath(s1);
    oD2.rmpath(s2);
    oD3.rmpath(s3);

    QString sBase = QString(s_CurrentPath + "/BASEDIR");
    QDir oDir(sBase);
    oDir.rmpath(sBase);
}

void DirManagerTest::cleanupTestCase()
{



}

