/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "OrderTest.h"
#include <Customer.h>
#include <crmglobaldefs.h>
#include <CRMDBFields.h>
#include <OrderWnd.h>
OrderTest::OrderTest(CRMApplication *pApp, QObject *parent) :
    testcore::BaseTest(pApp, parent)
{

}

void OrderTest::initTestCase()
{
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_CLIENTORDER, FDNM_CRM_CLIENTORDER_CUSTOMERID, "8008000");
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_CUSTOMER, FDNM_CRM_CUSTOMER_PHONE, "8008000");
    CreateCustomer();
}

void OrderTest::DoAddNewOrderTest()
{

    std::shared_ptr<Customer> pCustomer  = Entity::FindInstaceEx<Customer>("8008000", ENTITY_CUSTOMER);

    OrderWnd* pWnd = p_App->CreateWndEx<OrderWnd>(WND_ORDER);
    pWnd->InitializeWithCustomer(pCustomer);


    pWnd->p_Ctrl->p_SerSelector->cmb_ServiceType->setCurrentIndex( pWnd->p_Ctrl->p_SerSelector->cmb_ServiceType->findText("Air Conditioners"));
    pWnd->p_Ctrl->p_SerSelector->cmb_ServiceType->activated("Air Conditioners");
    pWnd->p_Ctrl->p_SerSelector->cmb_ServiceName->setCurrentText("Singer Split Type 900BTU");
    pWnd->p_Ctrl->p_SerSelector->cmb_ServiceName->activated("Singer Split Type 900BTU");


    QTest::mouseClick(pWnd->p_Ctrl->btn_Done, Qt::MouseButton::LeftButton);
    QList<std::shared_ptr<Entity>> lstOrder = Entity::FindInstances(FDNM_CRM_CLIENTORDER_CUSTOMERID, "8008000", TBLNM_CRM_CLIENTORDER);

    QVERIFY(lstOrder.size() == 1);

    std::shared_ptr<Order> pOrder = std::static_pointer_cast<Order>(lstOrder.first());

    QString sOrderID = pOrder->GetOrderID();

    QCOMPARE(pOrder->GetCustomerID(), QString("8008000"));

    QList<std::shared_ptr<Entity>> lstTasks = Entity::FindInstances(FDNM_CRM_TASK_ORDERID, sOrderID, TBLNM_CRM_TASK);

    QVERIFY(lstTasks.size() == 1);

    std::shared_ptr<Task> pTask = std::static_pointer_cast<Task>(lstTasks.first());
    QCOMPARE(pTask->GetData(FDNM_CRM_TASK_ORDERID).toString(), sOrderID);

    Connection::GetInstance()->DeleteRow(TBLNM_CRM_TASK, FDNM_CRM_TASK_ORDERID, sOrderID);
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_CLIENTORDER, FDNM_CRM_CLIENTORDER_ORDERID, sOrderID);
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_CUSTOMER, FDNM_CRM_CUSTOMER_PHONE, "8008000");
}

void OrderTest::cleanupTestCase()
{

}

void OrderTest::CreateCustomer()
{
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_CUSTOMER, FDNM_CRM_CUSTOMER_PHONE, "8008000");
    std::shared_ptr<Customer> pCustomer = std::static_pointer_cast<Customer>(p_App->GetEntityFactory()->CreateEntity(ENTITY_CUSTOMER));

    pCustomer->AddData(FDNM_CRM_CUSTOMER_PHONE , 8008000);
    pCustomer->AddData(FDNM_CRM_CUSTOMER_NAME , "TESTUSERNAME");
    pCustomer->AddData(FDNM_CRM_CUSTOMER_ADDRESS , "12,12, New York City");
    pCustomer->AddData(FDNM_CRM_CUSTOMER_EMAIL , "def@gmail.com");
    pCustomer->AddData(FDNM_CRM_CUSTOMER_OTHER , "");
    pCustomer->AddData(FDNM_CRM_CUSTOMER_DATEOFBIRTH , "2012-2-3");
    pCustomer->AddData(FDNM_CRM_CUSTOMER_NID , "870851138v");
    pCustomer->AddData(FDNM_CRM_CUSTOMER_REGISTRATIONDATE , "2016-3-2");
    pCustomer->AddData(FDNM_CRM_CUSTOMER_REGISTEDBY , "Admins");

    pCustomer->Save();

    pCustomer = Entity::FindInstaceEx<Customer>("8008000", ENTITY_CUSTOMER);
    QCOMPARE(pCustomer->GetCustomerName(), QString("TESTUSERNAME"));
}





