/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef SERVICEGROUPTEST_H
#define SERVICEGROUPTEST_H

#include <QObject>
#include <BaseTest.h>
class ServiceGroupTest : public testcore::BaseTest
{
    Q_OBJECT
public:
    explicit ServiceGroupTest(CRMApplication* pApp, QObject *parent = 0);

signals:

private slots:
    void initTestCase();
    virtual void DoTest();
    void cleanupTestCase();
};

#endif // SERVICEGROUPTEST_H
