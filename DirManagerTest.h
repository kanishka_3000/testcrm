/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef DIRMANAGERTEST_H
#define DIRMANAGERTEST_H
#define private public
#include <QObject>
#include <testcore/BaseTest.h>
#include <feature/DirectoryManager.h>
#include <QtTest/QtTest>
#include <QTest>
class DirManagerTest : public testcore::BaseTest
{
    Q_OBJECT
public:
    explicit DirManagerTest(CRMApplication* pApp, QObject *parent = 0);

    feature::DirectoryManager* p_DirManager;
    QString s_CurrentPath;

signals:

private slots:
    void initTestCase();
    virtual void DoPathGenTest();
    virtual void DoPathCreationTest();
    virtual void OpneDirTest();
    void cleanupTestCase();
     void Clean();
};

#endif // DIRMANAGERTEST_H
