/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef SERVICETEST_H
#define SERVICETEST_H

#include <QObject>
#include <BaseTest.h>
#include <ServiceGroup.h>
class ServiceTest : public testcore::BaseTest
{
    Q_OBJECT
public:
    explicit ServiceTest(CRMApplication* pApp, QObject *parent = 0);

signals:
private:
    std::shared_ptr<ServiceGroup> p_ServiceGroup;
private slots:
    void initTestCase();
    virtual void DoTest();
    void cleanupTestCase();
};

#endif // SERVICETEST_H
