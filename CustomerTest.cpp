/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "CustomerTest.h"
#include <crmglobaldefs.h>
#include <Customer.h>
#include <Connection.h>
#include <CRMDBFields.h>
#include <QMessageBox>
CustomerTest::CustomerTest(CRMApplication *pApp, QObject *parent) :
    testcore::BaseTest(pApp, parent)
{


}

void CustomerTest::initTestCase()
{
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_CUSTOMER, FDNM_CRM_CUSTOMER_PHONE, "0785969495");

    ManageCustomerWnd* pWnd = p_App->CreateWndEx<ManageCustomerWnd>(WND_MNG_CUSTOMERS);
    QTest::keyClicks(pWnd->p_RegisterCtrl->txt_ContactNo,"0785969495" );
    QTest::keyClicks(pWnd->p_RegisterCtrl->txt_Name, "This is a test name3");
    QTest::keyClicks(pWnd->p_RegisterCtrl->txt_Address, "Test Address2");
    QTest::keyClick(pWnd->p_RegisterCtrl->txt_NationID, Qt::Key_Backspace);
    QTest::keyClicks(pWnd->p_RegisterCtrl->txt_NationID, "870851138v");
    QDate oDate;
    oDate.setDate(1997, 4, 6);
    pWnd->p_RegisterCtrl->dt_DOB->setDate(oDate);
    pWnd->p_RegisterCtrl->dt_RegistratinDate->setDate(oDate);

    QTest::mouseClick( pWnd->p_RegisterCtrl->btn_Save, Qt::MouseButton::LeftButton);
}

void CustomerTest::DoTest()
{

    std::shared_ptr<Customer> pCustoerm = Entity::FindInstaceEx<Customer>("0785969495", ENTITY_CUSTOMER);

    QCOMPARE(pCustoerm->GetContactNo() , 785969495);
    QCOMPARE(pCustoerm->GetAddress() , QString("Test Address2"));
    QCOMPARE(pCustoerm->GetCustomerName() , QString("This is a test name3"));
    QCOMPARE(pCustoerm->GetData(FDNM_CRM_CUSTOMER_EMAIL).toString() , QString("default@default.com"));
    QCOMPARE(pCustoerm->GetData(FDNM_CRM_CUSTOMER_REGISTEDBY).toString() , QString("Admin"));
    QCOMPARE(pCustoerm->GetData(FDNM_CRM_CUSTOMER_NID).toString() , QString("870851138v"));
    QCOMPARE(pCustoerm->GetData(FDNM_CRM_CUSTOMER_DATEOFBIRTH).toString() , QString("1997-04-06"));
    QCOMPARE(pCustoerm->GetData(FDNM_CRM_CUSTOMER_REGISTRATIONDATE).toString() , QString("1997-04-06"));

}

void CustomerTest::cleanupTestCase()
{
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_CUSTOMER, FDNM_CRM_CUSTOMER_PHONE, "0785969495");
}

