/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef CUSTOMERTEST_H
#define CUSTOMERTEST_H

#include <QObject>
#include <QtTest/QtTest>
#include <QTest>
#include <CRMApplication.h>
#include <ManageCustomerWnd.h>
#include <BaseTest.h>
class CustomerTest : public testcore::BaseTest
{
    Q_OBJECT
public:
    explicit CustomerTest(CRMApplication* pApp, QObject *parent = 0);

private slots:
    void initTestCase();
    virtual void DoTest();
    void cleanupTestCase();
};

#endif // CUSTOMERTEST_H
