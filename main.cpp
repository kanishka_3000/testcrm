/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/


#include <QApplication>
#include <QtTest>
#include <CustomerTest.h>
#include <CRMObjecFactory.h>
#include <CRMApplication.h>
#include <OrderTest.h>
#include <PaymentTest.h>
#include <ServiceGroupTest.h>
#include <ServiceTest.h>
#include <BaseTestSummerizer.h>
#include <LoginWnd.h>
#include <DirManagerTest.h>

#define private public

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    CRMObjecFactory oFactory;
    CRMApplication oApp;
    oApp.Initialize(&a,&oFactory);

    testcore::BaseTestSummerizer oTestSummerizer;

//    CustomerTest oCustomerTest(&oApp);
//    oTestSummerizer.Register(&oCustomerTest);

//    OrderTest oOrderTest(&oApp);
//    oTestSummerizer.Register(&oOrderTest);

//    PaymentTest oPaymentTest(&oApp);
//    oTestSummerizer.Register(&oPaymentTest);

//    ServiceGroupTest oServiceGropTest(&oApp);
//    oTestSummerizer.Register(&oServiceGropTest);

//    ServiceTest oServiceTest(&oApp);
//    oTestSummerizer.Register(&oServiceTest);

    DirManagerTest oDirTest(&oApp);
    oTestSummerizer.Register(&oDirTest);

    oApp.p_LoginWindow->OnLogin("12", "Admin");

    return a.exec();
}
