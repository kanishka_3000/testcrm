/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ServiceGroupTest.h"
#include <ManageServicesGroupWnd.h>
#include <crmglobaldefs.h>
#include <CRMDBFields.h>
#include <ServiceGroup.h>
ServiceGroupTest::ServiceGroupTest(CRMApplication *pApp, QObject *parent) :
    testcore::BaseTest(pApp, parent)
{

}

void ServiceGroupTest::initTestCase()
{
    ManageServicesGroupWnd* pWnd = p_App->CreateWndEx<ManageServicesGroupWnd>(WND_ADD_SERVICE_GROUP);
    QTest::keyClicks(pWnd->p_AddServiceGroup->txt_ServiceGroupName, "TESTGROup");
    QTest::keyClicks(pWnd->p_AddServiceGroup->txt_Description , "Test Desc");
    pWnd->p_AddServiceGroup->cmb_State->setCurrentIndex(0);

    QTest::mouseClick(pWnd->p_AddServiceGroup->btn_Save, Qt::MouseButton::LeftButton);
}

void ServiceGroupTest::DoTest()
{
    QList<std::shared_ptr<Entity>> lstGroups = Entity::FindInstances(FDNM_CRM_SERVICEGROUP_NAME, "TESTGROup", TBLNM_CRM_SERVICEGROUP);
    QVERIFY(lstGroups.size() == 1);
    std::shared_ptr<ServiceGroup> pSerg = std::static_pointer_cast<ServiceGroup>(lstGroups.first());

    QCOMPARE(pSerg->GetServiceGroupName(), QString("TESTGROup"));
    QCOMPARE(pSerg->GetData(FDNM_CRM_SERVICEGROUP_DESCRIPTION).toString(), QString("Test Desc"));
    QVERIFY(!pSerg->GetServiceGroupID().isEmpty());
}

void ServiceGroupTest::cleanupTestCase()
{
    Connection::GetInstance()->DeleteRow(TBLNM_CRM_SERVICEGROUP, FDNM_CRM_SERVICEGROUP_NAME, "TESTGROup");
}

