#-------------------------------------------------
#
# Project created by QtCreator 2016-04-30T16:30:01
#
#-------------------------------------------------

QT       += core gui testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CRMTest
TEMPLATE = app

QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage  -O0

INCLUDEPATH += "/home/kanishka/Projects/CRM"
DEPENDPATH += "/home/kanishka/Projects/CRM"
INCLUDEPATH += "testcore"
include(/home/kanishka/Projects/CRM/CRM.pro)

SOURCES += main.cpp\
    CustomerTest.cpp \
    testcore/BaseTest.cpp \
    OrderTest.cpp \
    PaymentTest.cpp \
    ServiceGroupTest.cpp \
    ServiceTest.cpp \
    testcore/BaseTestSummerizer.cpp \
    DirManagerTest.cpp

HEADERS += \
    CustomerTest.h \
    testcore/BaseTest.h \
    OrderTest.h \
    PaymentTest.h \
    ServiceGroupTest.h \
    ServiceTest.h \
    testcore/BaseTestSummerizer.h \
    DirManagerTest.h


LIBS += \
    -lgcov

