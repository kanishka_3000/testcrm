/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ORDERTEST_H
#define ORDERTEST_H

#include <QObject>
#include <BaseTest.h>
#include <CRMApplication.h>
class OrderTest : public testcore::BaseTest
{
    Q_OBJECT
public:
    explicit OrderTest(CRMApplication* pApp, QObject *parent = 0);
protected:
    void CreateCustomer();
signals:

private slots:
    void initTestCase();
    void init(){}
    void DoAddNewOrderTest();
    void cleanup(){}
    void cleanupTestCase();
};

#endif // ORDERTEST_H
